const express = require('express');

const app = express();

app.use('/spinner', express.static(`${__dirname}/dist`));

app.listen(8000, () => { console.log('spinner server listening on port 8000'); });
