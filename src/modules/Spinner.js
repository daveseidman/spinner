export default class Spinner {
  constructor() {
    this.element = document.createElement('div');
    this.element.className = 'spinner';

    this.wheel = document.createElement('img');
    this.wheel.src = 'assets/img/game-of-life.png';

    this.arrow = document.createElement('img');
    this.arrow.src = 'assets/img/game-of-life-arrow.png';

    this.element.appendChild(this.wheel);
    this.element.appendChild(this.arrow);
  }


  update(angle) {
    this.wheel.style.transform = `rotateZ(${angle}deg)`;
  }
}
