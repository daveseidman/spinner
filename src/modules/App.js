import Accelerometer from './Accelerometer';
import Spinner from './Spinner';

export default class App {
  constructor() {
    this.update = this.update.bind(this);
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.element = document.createElement('div');
    this.element.className = 'app';

    this.accelerometer = new Accelerometer();
    this.spinner = new Spinner();

    this.element.appendChild(this.spinner.element);
    // this.element.appendChild(this.accelerometer.element);

    this.update();
  }

  listen() {
    window.addEventListener('resize', this.resize);
  }

  update() {
    this.spinner.update(this.accelerometer.angle);
    requestAnimationFrame(this.update);
  }

  resize() {

  }
}
