export default class Accelerometer {
  constructor() {
    // this.element = document.createElement('div');
    // this.element.className = 'accelerometer';
    // this.element.innerText = 'angle';
    this.update = this.update.bind(this);

    this.angle = 0;
    this.listen();
  }

  listen() {
    window.addEventListener('deviceorientation', this.update);
  }

  update({ alpha }) {
    // this.element.innerText = alpha.toPrecision(3);
    this.angle = alpha;
    // this.element.innerText = 'here';
  }
}
