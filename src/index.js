import App from './modules/App';
import './index.scss';

const app = new App();
document.body.appendChild(app.element);
